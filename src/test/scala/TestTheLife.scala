import org.scalatest.FunSuite

class TestTheLife extends FunSuite {
  val inputBody = Array(
    Array(0, 1, 0),
    Array(1, 1, 0),
    Array(0, 0, 0),
    Array(1, 0, 0)
  )

  test("TheLife.getNumberOfAliveNeighbors") {
    assert(TheLife.getNumberOfAliveNeighbors(inputBody, 0, 0) === 3)
    assert(TheLife.getNumberOfAliveNeighbors(inputBody, 1, 0) === 2)
    assert(TheLife.getNumberOfAliveNeighbors(inputBody, 2, 0) === 3)
    assert(TheLife.getNumberOfAliveNeighbors(inputBody, 2, 2) === 1)
  }

  test("TheLife.getNextLifeGeneration") {
    val nextLifeGeneration = TheLife.getNextLifeGeneration(inputBody)
    assert(nextLifeGeneration.length === inputBody.length)
    assert(nextLifeGeneration(0) === Array(1, 1, 0))
    assert(nextLifeGeneration(1) === Array(1, 1, 0))
    assert(nextLifeGeneration(2) === Array(1, 1, 0))
    assert(nextLifeGeneration(3) === Array(0, 0, 0))
  }

}
