package readinput

import scala.io.Source

class InitialFieldReader(path: String) {
  def readInitialField(): Array[Array[Int]] = {
    Source
      .fromFile(path)
      .getLines
      .map(str => str.toArray)
      .map(array => array.map(_.asDigit))
      .toArray
  }
}

object InitialFieldReader extends App {
  val reader = new InitialFieldReader("/home/oleksandr/IdeaProjects/game-of-life/src/main/resources/field_1.txt")
  reader.readInitialField()
}