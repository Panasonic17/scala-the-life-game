import readinput.InitialFieldReader

object LifeGame extends App {
  val reader = new InitialFieldReader("/home/oleksandr/IdeaProjects/game-of-life/src/main/resources/field_2.txt")

  var body: Array[Array[Int]] = reader.readInitialField()

  while (true) {
    body = TheLife.getNextLifeGenerationAndPrint(body, FieldPrinter.printField)
    Thread.sleep(1000)
  }
}
