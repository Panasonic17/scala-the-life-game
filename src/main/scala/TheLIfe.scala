object TheLife {
  val indexPattern: Array[(Int, Int)] =
    Array(
      (-1, -1), (-1, 0), (-1, 1),
      (0, -1), (0, 1),
      (1, -1), (1, 0), (1, 1))

  def getNextLifeGenerationAndPrint(body: Array[Array[Int]], printer: Array[Array[Int]] => Unit): Array[Array[Int]] = {
    printer(body)
    getNextLifeGeneration(body)
  }

  def getNextLifeGeneration(body: Array[Array[Int]]): Array[Array[Int]] = {
    val height = body.length
    val width = body(0).length
    val newField: Array[Array[Int]] = new Array[Array[Int]](height)
    for (y <- 0 until height) {
      val row = new Array[Int](width)
      for (x <- 0 until width) {
        val numberOFAliveNeighborsCells = getNumberOfAliveNeighbors(body, y = y, x = x)
        row(x) = numberOFAliveNeighborsCells match {
          case 3 => 1
          case 2 if body(y)(x)==1 => 1
          case _ => 0
        }
      }
      newField(y) = row
    }
    newField
  }

  def getNumberOfAliveNeighbors(body: Array[Array[Int]], y: Int, x: Int): Int = {
    indexPattern.map(tuple => (y + tuple._1, x + tuple._2))
      .map(tuple => getNeighborhoodValue(body, tuple._1, tuple._2)).sum
  }

  def getNeighborhoodValue(body: Array[Array[Int]], y: Int, x: Int): Int = {
    (x, y) match {
      case x if x._2 >= body.length => 0
      case x if x._1 >= body(0).length => 0
      case x if x._1 < 0 => 0
      case x if x._2 < 0 => 0
      case x => body(x._2)(x._1)
    }
  }
}