object FieldPrinter extends App {
  def printField(body: Array[Array[Int]]): Unit = {
    for (arr <- body) {
      for (cell <- arr) {
        print(cell)
      }
      println()
    }
    println()
  }
}